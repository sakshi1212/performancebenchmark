const { postgres } = require('./environment');

const configDefaults = {
  dialect: 'postgres',
};

const config = {
  development: {
    username: postgres.username,
    password: postgres.password,
    database: postgres.name,
    host: postgres.host,
    ...configDefaults,
  },
  staging: {
    username: postgres.username,
    password: postgres.password,
    database: postgres.name,
    host: postgres.host,
    ...configDefaults,
  },
  production: {
    username: postgres.username,
    password: postgres.password,
    database: postgres.name,
    host: postgres.host,
    ...configDefaults,
  },
};

console.log(`Running sequelize-cli on host: ${config.host}`);

module.exports = {
  ...config,
};
