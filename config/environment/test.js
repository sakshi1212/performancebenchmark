'use strict';

module.exports = {
  env: 'test',
  port: '8080',
  postgres: {
    name: "performanceBenchmarkTest",
    host: "127.0.0.1",
    username: "root",
    password: "123123123",
  },
};