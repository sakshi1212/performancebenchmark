import React from 'react';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';
import { prop } from 'styled-tools'

const styles = css`
  color: ${prop('palette') || 'dark-grey'}
`;

const Heading = styled(
  ({ level, children, ...props }) =>
    React.createElement(`h${level}`, props, children),
)`
  ${styles};
`;

Heading.propTypes = {
  level: PropTypes.number,
  children: PropTypes.node,
  palette: PropTypes.string,
  reverse: PropTypes.bool,
  fontFamily: PropTypes.string,
};

Heading.defaultProps = {
  level: 1,
  palette: 'primary',
};

export default Heading;