import React from 'react';
import { Button as SUIButton } from 'semantic-ui-react';
import styled from 'styled-components';

const StyledButton = styled(SUIButton)`
  &&& {
    background-color: orange;
  }
`;

const Button = (props) => {
  return (
    <StyledButton {...props} />
  );
};

export default Button;