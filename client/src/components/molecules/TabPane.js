import React from 'react'
import { Tab } from 'semantic-ui-react'

const TabPane = ({ content, attached }) => (
  <Tab.Pane 
    attached={attached}
  >
    {content}
  </Tab.Pane>
)

export default TabPane;