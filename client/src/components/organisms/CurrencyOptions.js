import React, { Component } from 'react'
import { Form } from 'semantic-ui-react'
import Radio from '../atoms/Radio'
import styled from 'styled-components'

const StyledForm = styled(Form)`
  &&& {
    width: 20%;
    display: flex;
    justify-content: flex-end;
  }
`

class CurrencyOptions extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: this.props.currencySelection,
    }
  }

  handleChange = (e, { value }) => this.setState({ value }, this.props.handleCurrencyChange(value))
  
  render() {
    return (
      <StyledForm>
        <Form.Field>
          <Radio
            label='SGD'
            name='radioGroup'
            value='sgd'
            checked={this.state.value === 'sgd'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='USD'
            name='radioGroup'
            value='usd'
            checked={this.state.value === 'usd'}
            onChange={this.handleChange}
          />
        </Form.Field>
      </StyledForm>
    )
  }
}

export default CurrencyOptions;