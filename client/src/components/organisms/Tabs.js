import React from 'react';
import { Tab } from 'semantic-ui-react';
import TabPane from '../molecules/TabPane';

const Tabs = ({ tabs }) => {

  const panes = tabs.map(t => {
    return {
      menuItem: t.tabTitle,
      render: () => <TabPane 
                      attached={false} 
                      content={t.tabContent}
                    />,
    }
  });

  return (
    <Tab 
      menu={{ secondary: true, pointing: true }}
      panes={panes}
    />
  );
};

export default Tabs
