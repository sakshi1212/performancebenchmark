import React, { Component } from 'react'
import styled from 'styled-components'
import Text from '../atoms/Text' 
import LineChart from '../organisms/LineChart'

const LineChartWrapper = styled.div`
  background: #071F37;
  border-radius: 5px; 
  padding: 20px;
`

class ResultsGraph extends Component {
  render() {
    return (
      <LineChartWrapper>
        <Text level={1} palette={'white'}>Portfolio value based on gross returns</Text>
        <Text level={2} palette={'white'}>Gross returns and exchange rates sourced from MockData as of 2nd May 2019</Text>
        <LineChart 
          chartData={this.props.chartData}
        />
      </LineChartWrapper>
    )
  }
}

export default ResultsGraph;