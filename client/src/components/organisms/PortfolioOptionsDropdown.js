import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'

class PortfolioOptionsDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultValue: this.props.defaultValue,
    }
  }

  render() {
    return (<Dropdown
      placeholder={this.props.placeholder}
      fluid
      selection
      options={this.props.dropdownOptions}
      onChange={this.props.handleDropdownChange}
      // defaultValue={this.props.dropdownDefaultValue}
    />);
  }
}

export default PortfolioOptionsDropdown