import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer
} from 'recharts';
import styled from 'styled-components';
import startCase from 'lodash/startCase';


const TooltipWrapper = styled.div`
  background: white;
  padding: 6px;
  border-radius: 3px;
`

const ToolTipDate = styled.div`
  color: dark-grey;
  font-weight: bold;
`
const ToolTipName = styled.div`
  font-weight: bold;
`

const CustomTooltip = ({ active, payload, label }) => {
  if (active) {
    return (
      <TooltipWrapper>
        <ToolTipDate>{label}</ToolTipDate>
        <ToolTipName style={{color: `${payload[0].stroke}`}}>{`${startCase(payload[0].name)}`}</ToolTipName>
        <p>{payload[0].value}</p>
        <ToolTipName style={{color: `${payload[1].stroke}`}}>{`${startCase(payload[1].name)}`}</ToolTipName>
        <p>{payload[1].value}</p>
      </TooltipWrapper>
    );
  }
  return null;
};

class PlottedLineChart extends PureComponent {

  render() {
    return (
      <ResponsiveContainer
        height={400}
        width={'100%'}
      >
        <LineChart
          width={500}
          height={300}
          data={this.props.chartData}
          margin={{
            top: 5, right: 30, left: 20, bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" stroke="#aab8c2" vertical={false}/>
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip content={<CustomTooltip />} />/>
          <Legend />
          <Line type="monotone" dataKey="stashAway14%" stroke="#8884d8" activeDot={{ r: 8 }} />
          <Line type="monotone" dataKey="selectedPortfolio" stroke="#82ca9d" />
        </LineChart>
      </ResponsiveContainer>

    );
  }
}

export default PlottedLineChart;
