import React, { Component } from 'react'
import { Form } from 'semantic-ui-react'
import Radio from '../atoms/Radio'
import styled from 'styled-components'

const StyledForm = styled(Form)`
  &&& {
    width: 80%;
    display: flex;
  }
`

class TimeDurationOptions extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: this.props.timeSelection,
    }
  }

  handleChange = (e, { value }) => this.setState({ value }, this.props.handleTimeChange(value))
  
  render() {
    return (
      <StyledForm>
        {/* <Form.Field>
          Selected value: <b>{this.state.value}</b>
        </Form.Field> */}
        <Form.Field>
          <Radio
            label='1 Month'
            name='radioGroup'
            value='1M'
            checked={this.state.value === '1M'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='6 Months'
            name='radioGroup'
            value='6M'
            checked={this.state.value === '6M'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='Year To Date'
            name='radioGroup'
            value='yearToDate'
            checked={this.state.value === 'yearToDate'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='1 Year'
            name='radioGroup'
            value='1Y'
            checked={this.state.value === '1Y'}
            onChange={this.handleChange}
          />
        </Form.Field>
      </StyledForm>
    )
  }
}

export default TimeDurationOptions;