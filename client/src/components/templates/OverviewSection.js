import React, { Component } from 'react'
import styled from 'styled-components'
import Heading from '../atoms/Heading'
import Text from '../atoms/Text'
import TimeDurationOptions from '../organisms/TimeDurationOptions'
import CurrencyOptions from '../organisms/CurrencyOptions'
import ResultsGraph from '../organisms/ResultsGraph'
import PortfolioOptionsDropdown from '../organisms/PortfolioOptionsDropdown'
import axios from 'axios'
import groupBy from 'lodash/groupBy'
import find from 'lodash/find'
import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty'
import moment from 'moment'
import filter from 'lodash/filter';

const PortfolioBenchmarkWrapper = styled.div`
`;

const ComparisionOption = styled.div`
  height: 100px;
  display: flex;
  margin: 20px 0px;
  position: relative;
`

const BenchmarkingOptions = styled.div`
  padding: 20px 0px;
  display: flex;
`

const IndexOption = styled.div`
  width: 50%;
  display: flex;
  align-items: center;
  padding: 0px 40px;
`

const OwnIndexWrapper = styled(IndexOption)`
  background: #F7F7F7;
  justify-content: right;
  text-align: left;
  border-radius: 5px 0px 0px 5px;
`

const OtherIndexWrapper = styled(IndexOption)`
  background: #EBECEF;
  justify-content: center;
  border-radius: 0px 5px 5px 0px;
`

const StyledCurrencyOptions = styled(CurrencyOptions)`
  width: 20%;
  display: flex;
  justify-content: flex-end;
`

const OverviewWrapper = styled.div`
  padding: 40px 0px;
`  

const VS = styled.div`
  background: #3D81D7;
  width: 30px;
  height: 30px;
  color: white;
  font-size: 18px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  left: 49%;
`

class OverviewSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: [],
      chartStructuredData: [],
      dropdownOptions: [],
      dropdownDefaultValue: '2',
      timeSelection: '1M',
      currencySelection: 'sgd',
      comparePortfolioId: null,
    }
  }

  componentDidMount() {
    this.getPortfolioOptions();
    this.getChartData();
  }

  getPortfolioOptions = () => {
    axios.get('/portfolios')
    .then(({ data }) => {
      const filteredOptions = filter(data, d => d.id !== 1);
      const dropdownOptions = filteredOptions.map(op => {
        return {
          key: op.id,
          text: op.portfolioDisplayName,
          value: op.id,
        }
      });
      this.setState({
        dropdownOptions,
      });
    })
    .catch((error) => {
      console.log(error);
    });
  }

  getTimeFilters = () => {
    const currentTimeSelection = this.state.timeSelection;
    const today = moment().format('YYYY-MM-DD');
    switch(currentTimeSelection) {
      case '1M': return {
        startDate: moment().subtract(1, 'month').format('YYYY-MM-DD'),
        endDate: today,
      };
      case '6M': return {
        startDate: moment().subtract(6, 'month').format('YYYY-MM-DD'),
        endDate: today,
      };
      case 'yearToDate': return {
        startDate: moment().startOf('year').format('YYYY-MM-DD'),
        endDate: today,
      };
      case '1Y': return {
        startDate: moment().subtract(1, 'year').format('YYYY-MM-DD'),
        endDate: today,
      };
      default: return {
        startDate: moment().subtract(1, 'month').format('YYYY-MM-DD'),
        endDate: today,
      };
    }
  }

  getChartData = () => {
    const filters = this.getTimeFilters();
    Promise.all([
      axios.get('/portfolios/1/grossReturns', {
        params: {
          ...filters,
        }
      }),
      this.state.comparePortfolioId ? 
        axios.get(`/portfolios/${this.state.comparePortfolioId}/grossReturns`, {
          params: {
            ...filters,
          }
        }) : null,
    ])
      .then(([resp1, resp2]) => {
        const data1 = get(resp1, 'data', []);
        const data2 = get(resp2, 'data', []);
        const groupedByDate = groupBy([...data1, ...data2], 'date');
        this.setState({
          chartData: groupedByDate,
        }, this.setDataInSelectedCurrency);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  setDataInSelectedCurrency = () => {
    if (!isEmpty(this.state.chartData)) {
      const groupedByDate = this.state.chartData;
      const currentCurrencySelection = this.state.currencySelection;
      const structuredData = Object.keys(groupedByDate).map(date => {
        return {
          name: date,
          'stashAway14%': (get(find(groupedByDate[date], ['portfolioId', 1]), currentCurrencySelection, 0)).toFixed(2),
          'selectedPortfolio': (get(find(groupedByDate[date], ['portfolioId', this.state.comparePortfolioId]), currentCurrencySelection, 0)).toFixed(2),
        }
      });
      this.setState({
        chartStructuredData: structuredData,
      });
    } else {
      this.getChartData();
    }
  }

  handleTimeChange = (value) => {
    this.setState({
      timeSelection: value,
    }, this.getChartData)
  }

  handleCurrencyChange = (value) => {
    this.setState({
      currencySelection: value,
    }, this.setDataInSelectedCurrency);
  }

  handleDropdownChange = (e, { value }) => {
    this.setState({
      comparePortfolioId: value,
    }, this.getChartData);
  }

  render() {
    return (
      <OverviewWrapper>
        <Heading level={3}>Portfolio Benchmark</Heading>
        <PortfolioBenchmarkWrapper> 
   
          <ComparisionOption>
            <OwnIndexWrapper>
              <div>
                <VS>vs</VS>
                <Text level={3} palette={'dark-grey'}>General Investing</Text>
                <Text level={2} palette={'#3D81D7'} weight={'bold'}>Stash Away Index</Text>
              </div>
            </OwnIndexWrapper>
            <OtherIndexWrapper>
              <PortfolioOptionsDropdown 
                dropdownOptions={this.state.dropdownOptions}
                dropdownDefaultValue={this.state.dropdownDefaultValue}
                handleDropdownChange={this.handleDropdownChange}
                placeholder={'Which benchmark do you want to compare'}
              />
            </OtherIndexWrapper>
          </ComparisionOption>
  
  
          <BenchmarkingOptions>
            <TimeDurationOptions 
              timeSelection={this.state.timeSelection}
              handleTimeChange={this.handleTimeChange}
            />
            <StyledCurrencyOptions 
              currencySelection={this.state.currencySelection}
              handleCurrencyChange={this.handleCurrencyChange}
            />
          </BenchmarkingOptions>
  
          <ResultsGraph 
            chartData={this.state.chartStructuredData}
          />
  
  
        </PortfolioBenchmarkWrapper>
      </OverviewWrapper>
    )
  }

}

export default OverviewSection;