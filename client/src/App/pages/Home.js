import React, { Component } from 'react';
import Tabs from '../../components/organisms/Tabs';
import OverviewSection from '../../components/templates/OverviewSection'
import Heading from '../../components/atoms/Heading';
import styled from 'styled-components';
import { Icon } from 'semantic-ui-react'

const HeadingWraper = styled.div`
  padding: 40px 100px;
  background: #071f37;
  text-align: left;
`

const TabsWrapper = styled.div`
  padding: 20px 100px;
`

class Home extends Component {
  render() {
    const tabs = [
      {
        tabTitle: 'Overview',
        tabContent: <OverviewSection />
      },
      {
        tabTitle: 'Assests',
        tabContent: 'Assests coming soon'
      },
      {
        tabTitle: 'Production',
        tabContent: 'Production coming soon'
      },
      {
        tabTitle: 'About Portfolio',
        tabContent: 'About Portfolio coming soon'
      },
    ]

    return (
    <div className="App">
      <HeadingWraper>
        <Heading level={3} palette={'#36ADA9'}><Icon name="arrow left" />Overview</Heading>
        <Heading level={1} palette={'white'}>General Investing</Heading>
      </HeadingWraper>
      <TabsWrapper>
        <Tabs tabs={tabs}/>
      </TabsWrapper>
    </div>
    );
  }
}
export default Home;