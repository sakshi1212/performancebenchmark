const { Op } = require('sequelize');
const moment = require('moment');
const isNil = require('lodash/isNil');

const getRangeQuery = (key, { start, end } = {}) => {
  if (isNil(start) && isNil(end)) return {};
  if (isNil(end)) return { [key]: { [Op.gte]: start } };
  if (isNil(start)) return { [key]: { [Op.lte]: end } };
  return { [key]: { [Op.between]: [start, end] } };
};

const getTimeRangeQuery = (key, { startDate, endDate }) => {
  const validator = date => !isNil(date) && moment(date).isValid();
  startDate = validator(startDate) ? moment(startDate).toISOString() : undefined;
  endDate = validator(endDate) ? moment(endDate).toISOString() : undefined;

  return getRangeQuery(key, {
    start: startDate,
    end: endDate,
  });
};

module.exports = {
  getTimeRangeQuery,
};