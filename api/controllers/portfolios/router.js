const express = require('express');
const controller = require('./controller');

const router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/:id/grossReturns', controller.getGrossReturns);

module.exports = router;
