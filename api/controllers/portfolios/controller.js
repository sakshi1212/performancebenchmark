const asyncMW = require('../../../middleware/async');
const omitBy = require('lodash/omitBy');
const isNil = require('lodash/isNil');
const { getTimeRangeQuery } = require('../../utils/seqeulize');

exports.index = asyncMW(async (req, res) => {
  try {
    const portfolios = await DB.Portfolio.findAll();
    return res.status(200).send(portfolios);
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
});

exports.show = asyncMW(async (req, res) => {
  const { id } = req.params;
  try {
    const portfolio = await DB.Portfolio.findOne({
      where: {
        id,
      },
      rejectOnEmpty: new HttpError(404, `Portfolio with id ${id} not found`),
    });
    return res.status(200).send({
      data: portfolio,
    });
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
});


exports.getGrossReturns = asyncMW(async (req, res) => {
  const { id } = req.params;
  const { startDate, endDate } = req.query;
  try {
    const portfolio = await DB.Portfolio.findOne({
      where: {
        id,
      },
      rejectOnEmpty: new HttpError(404, `Portfolio with id ${id} not found`),
    });

    const grossReturns = await portfolio.getGrossReturns({
      where: omitBy({
        ...getTimeRangeQuery('date', { startDate, endDate }),
      }, isNil),
      order: [['date', 'asc']],
    });

    return res.status(200).send(grossReturns);

  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
});

