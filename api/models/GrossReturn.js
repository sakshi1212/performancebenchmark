const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;

class GrossReturn extends Sequelize.Model {

  static init(sequelize) {
    return super.init(
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER,
        },
        portfolioId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'portfolios',
            key: 'id',
          },
        },
        date: {
          type: DataTypes.DATEONLY,
          allowNull: false
        },
        usd: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
        sgd: {
          type: DataTypes.DOUBLE,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'grossReturns',
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.Portfolio);
  }
}

module.exports = GrossReturn;
