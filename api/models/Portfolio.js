const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;

class Portfolio extends Sequelize.Model {

  static init(sequelize) {
    return super.init(
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER,
        },
        portfolioName: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        portfolioDisplayName: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        source: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'portfolios',
      }
    );
  }

  static associate(models) {
    this.hasMany(models.GrossReturn);
  }
}

module.exports = Portfolio;
