const portfolios = [
  {
    portfolioName: 'StashAway 14%',
    portfolioDisplayName: 'StashAway Risk Index 14%',
    source: 'Mock',
  },
  {
    portfolioName: '60% Stocks - 40% Bonds',
    portfolioDisplayName: '60% Stocks - 40% Bonds',
    source: 'Mock',
  },
  {
    portfolioName: '60% Stocks - 40% Bonds',
    portfolioDisplayName: '80% Stocks - 20% Bonds',
    source: 'Mock',
  },
]

module.exports = {
  up: (queryInterface) => {
    const now = new Date();
    const timestamps = {
      createdAt: now,
      updatedAt: now,
    };
    return queryInterface.bulkInsert('portfolios',
      portfolios.map(portfolio => ({
        ...portfolio,
        ...timestamps })),
      {},
      );
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('portfolios', null, {});                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
  },
};

