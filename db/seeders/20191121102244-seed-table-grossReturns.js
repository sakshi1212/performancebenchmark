const flatten = require('lodash/flatten');

module.exports = {
  up: (queryInterface) => {

    const now = new Date();
    const timestamps = {
      createdAt: now,
      updatedAt: now,
    };
    let datesOfYear = [];
    for (let d = new Date(2018, 10, 1); d <= now; d.setDate(d.getDate() + 1)) {
      datesOfYear.push(new Date(d));
    }
    const portfolioIds = [1, 2, 3];
    let allGrossReturns = [];
    portfolioIds.forEach(pId => {
      const min = pId === 1 ? 225 : 150;
      let max = min + 1;
      const portfolioGrossReturns = datesOfYear.map(date => {
        const returnsUsd = Math.random() * (max - min) + min;
        const returnsSgd = returnsUsd * 1.36;
        max=max+1;
        return {
          portfolioId: pId,
          date,
          usd: returnsUsd,
          sgd: returnsSgd,
          ...timestamps,
        }
      })
      allGrossReturns.push(portfolioGrossReturns);
    });
    return queryInterface.bulkInsert('grossReturns',
      flatten(allGrossReturns),
      {},
    );
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('grossReturns', null, {});                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
  },
};